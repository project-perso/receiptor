'use strict';
import React from 'react';
import {connect} from 'react-redux';
import {logout} from '../data/actions/'

@connect()
export class PageLogout extends React.Component {
    constructor(props) {
        super(props);
    }
   
    render() {
        this.props.dispatch(logout());
        return null;
    }
}
