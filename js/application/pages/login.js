'use strict';

import { user } from '../data/api';
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { TextField, RaisedButton, Paper, Snackbar, CircularProgress, FlatButton } from 'material-ui';
import { login } from '../data/actions/';


@connect((store) => {
    return {
        is_editing: store.ui.is_editing,
        receipt: store.ui.current_receipt
    };
})
export class PageLogin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            mode: 'login',
            loading: false,
            username: '',
            password: ''
        };
    }

    componentWillReceiveProps(){
    }

    onSubmit(e){
        e.preventDefault();
        this.state.mode === 'login'? this.onLogin() : this.onRegister();
    }

    onLogin(){
        this.onChange('loading', null, null, true);
        return user
            .login(this.state.username, this.state.password)
            .then((username) => {
                this.props.dispatch(login(username));
                this.onChange('message', null, null, 'success');
                this.onChange('loading', null, null, false);
            })
            .catch((err) => {
                this.onChange('message', null, null, err.message);
                this.onChange('loading', null, null, false);
            });
    }
    onRegister(){
        this.onChange('loading', null, null, true);
        return user
            .register(this.state.username, this.state.password)
            .then((username) => {
                this.props.dispatch(login(username));
                this.onChange('message', null, null, 'success');
                this.onChange('loading', null, null, false);
            })
            .catch((err) => {
                if(err.status === 409) err.message = "already taken, sorry :/"
                this.onChange('message', null, null, err.message);
                this.onChange('loading', null, null, false);
            });
    }

    onChange(field, e, index, value){
        let state = Object.assign({}, this.state);
        state[field] = value || index;
        return this.setState(state);
    }


    render() {
        let submit = null;
        if(this.state.loading){
            submit = <CircularProgress color="white" size={26} style={{padding: '5px'}}/>;
        }else if(this.state.mode === 'login'){
            submit = "Login";
        }else{
            submit = "Register";
        }
        return (
            <div>
              <form onSubmit={this.onSubmit.bind(this)}>
                <div style={{width: "300px", margin: '0 auto 0 auto', paddingTop: '20%'}}>
                  <Paper style={{padding: '15px'}} zDepth={5}>
                    <TextField value={this.state.username} onChange={this.onChange.bind(this, 'username')} ref="username" hintText="Email" fullWidth={true} />
                    <TextField value={this.state.password} onChange={this.onChange.bind(this, 'password')} ref="password" hintText="Password" type="password" fullWidth={true} />
                    {
                        this.state.mode === 'login'?
                            <RaisedButton type="submit" label="Login" primary={true} style={{marginTop: '20px'}} fullWidth={true} />
                            :
                            <RaisedButton type="submit" label="Register" primary={true} style={{marginTop: '20px'}} fullWidth={true} />
                    }                    
                  </Paper>
                  {
                      this.state.mode === 'login'?
                          <FlatButton type="button" label="Register" onClick={this.onChange.bind(this, 'mode', null, null, 'register')} style={{float: 'right'}} disableTouchRipple={true} />
                          :
                          <FlatButton type="button" label="Login" onClick={this.onChange.bind(this, 'mode', null, null, 'login')} style={{float: 'right'}} disableTouchRipple={true} />
                  }
                </div>
              </form>
              <Snackbar
                open={this.state.message? true: false}
                message={this.state.message || ''}
                autoHideDuration={4000}
                onRequestClose={this.onChange.bind(this, 'message')}
                />
            </div>
        );
    }
}
