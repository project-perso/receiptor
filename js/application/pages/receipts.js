'use strict';

import React from 'react';
import { connect } from 'react-redux';
import Header from '../components/Header.jsx';
import Container from '../components/Container.jsx';
import TodoInput from '../components/TodoInput.jsx';
import ReceiptList from '../components/ReceiptList.jsx';
import StatusBar from '../components/StatusBar.jsx';
import CRUDReceipt from '../components/CrudReceipt.jsx';
import Filters from '../components/Filters.jsx';
import Settings from '../components/Settings.jsx';
import Statistics from '../components/Statistics.jsx';

@connect((store) => {
    return {
        user: store.user.username
    };
})
export class PageReceipts extends React.Component {

    constructor(props) {
        super(props);
    }
   
    render() {
        if(this.props.user){
            return (
                    <div>
                    <Header/>
                    <Container>
                    <Filters/>
                    <ReceiptList/>
                    </Container>
                    <CRUDReceipt/>
                    <Settings/>
                    <Statistics/>                    
                    </div>
            );
        }else{
            window.location.hash = "/login";
            return null;
        }
    }
}
