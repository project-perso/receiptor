let theme = {
    spacing: {
        iconSize: 24,
        desktopGutter: 24,
        desktopGutterMore: 32,
        desktopGutterLess: 16,
        desktopGutterMini: 8,
        desktopKeylineIncrement: 50,
        desktopDropDownMenuItemHeight: 32,
        desktopDropDownMenuFontSize: 15,
        desktopDrawerMenuItemHeight: 48,
        desktopSubheaderHeight: 48,
        desktopToolbarHeight: 56
    },
    fontFamily: 'Roboto, sans-serif',
    palette: {
        primary1Color: "#6b8d9f",
        primary2Color: "#44acde",
        primary3Color: "#ff0000",
        accent1Color: "#C2C8CC",
        accent2Color: "#ff0000", 
        accent3Color: "#ff0000", 
        textColor: "#4C6E80", 
        secondaryTextColor: "#7f949f", 
        alternateTextColor: "#ffffff", 
        canvasColor: "#ffffff", 
        borderColor: "#e1e7eb", 
        disabledColor: "#6b8d9f", 
        pickerHeaderColor: "#00ff00", 
        clockCircleColor: "#00ff00",
        shadowColor: "#9EA3AC",
    }
};
export default theme;
