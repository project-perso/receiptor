import React from 'react';
import PouchDB from 'pouchdb';
import auth from 'pouchdb-authentication';
import { connect } from 'react-redux';
import { importAllRemoteReceipts, importRemoteReceipts } from '../data/actions/';

@connect((store) => {
    return {
        user: store.user
    };
})
export class Api extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username: localStorage.getItem('username') || null,
            local: null,
            remote:  new PouchDB("http://127.0.0.1:5984/log"),
            sync: null
        };
    }

    componentWillMount(){
        if(this.state.username){
            //this.bootstrap(this.state.username);
        }
    }

    componentWillReceiveProps(props){
        if(props.user.username !== this.state.username){
            if(props.user.username === null){
                //this.destroy()
            }else{
                //this.bootstrap(props.user.username);
            }
        }
    }

    bootstrap(username){
        console.log("> START REPLICATION", username);
        if(!username) return;
        let db = this.state;
        db.username = username;
        if(db.sync) db.sync.cancel();
        if(db.remote) db.remote.close();
        if(db.local) db.local.close();
        
        db.remote = new PouchDB("http://127.0.0.1:5984/"+username);
        db.local = new PouchDB(username);
        
        db.sync = db.local.sync(db.remote, { live: true, retry: true, include_docs: true })
            .on('paused', () => {
                console.log("PAUSED");
                this.getReceipts()
                    .then((receipts) => this.props.dispatch(importAllRemoteReceipts(receipts)));
            })
            .on('change', (feed) => {
                console.log("FEED", feed)
                if(feed.direction === "pull"){                    
                }
            })
            .on('error', function (err) {
                console.log("ERR", err);
            });
        this.setState(db);
    }

    destroy(){
        let db = this.state;
        if(db.sync) db.sync.cancel();
        if(db.remote) db.remote.close();
        if(db.local){db.local.destroy()}
        this.setState({local: null, remote: new PouchDB("http://127.0.0.1:5984/log"), sync: null});
    }

    

    getReceipts(){
        return this.state.local.allDocs({include_docs: true})
            .then((data) => {
                return data.rows.map((row) => {
                    return row.doc;
                });
            });
    }
    putReceipt(receipt){
        receipt.type = 'receipt';
        return this.state.local.put(receipt);
    }
    updateReceipt(receipt){
        return this.state.db.local.get(receipt._id)
            .then((doc) => {
                receipt._rev = doc._rev;
                return this.state.local.put(receipt);
            });
    }
    deleteReceipt(receipt_id){
        return this.state.db.local.get(receipt_id)
            .then((doc) => {
                return this.state.local.remove(doc);
            });
    }

    
    render(){
        return (
            <div>
              { this.props.children }
            </div>
        );
    }
}
