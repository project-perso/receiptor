import PouchDB from 'pouchdb';
import auth from 'pouchdb-authentication';
import { login, importAllRemoteReceipts, logout, updateCategories, updateDefaultCurrency } from './actions/';
import { store } from './store';
import _ from 'lodash';

PouchDB.plugin(auth);

let db = {
    remote: null,
    local: null,
    sync: null
}
function _reset(){
    if(db.sync) db.sync.cancel();
    if(db.remote) db.remote.close();
    if(db.local) db.local.close();
    db = {
        remote: new PouchDB('http://127.0.0.1:5984/log'),
        local: null,
        sync: null
    }
}
_reset();


export const user = {
    login: function(username, password){
        return db.remote.login(username, password)
            .then((success) => new Promise((done) => {
                localStorage.setItem("username", username);
                store.dispatch(login(username));
                app.bootstrap(username);
                done(username);
            }));
    },
    logout: function(){
        localStorage.clear();
        db.remote.logout();
    },
    register: function(username, password){
        return db.remote.signup(username, password)
            .then(() => user.login(username, password))
            .catch(err => console.error(err));
    }
}

export const receipts = {
    get: function(){
        return db.local.query('query/receipts', {include_docs: true, attachments: true})
            .then((data) => {
                return data.rows.map((row) => {
                    return row.doc;
                });
            });
    },
    put: function(receipt){
        return db.local.put(receipt);
    },
    update: function(receipt){
        return db.local.get(receipt._id)
            .then((doc) => {
                receipt._rev = doc._rev;
                return db.local.put(receipt);
            });
    },
    delete: function(id){
        return db.local.get(id)
            .then((doc) => {
                return db.local.remove(doc);
            });
    }
}

export const settings = {
    get: function(key){
        return db.local.query('query/settings', {include_docs: true})
            .then((data) => {
                data = data.rows.map((row) => {
                    return row.doc;
                });
                for(let i=0, l=data.length; i<l; i++){
                    if(data[i]._id === key) return data[i].value;
                }
                return null;
            });
    },
    update: function(doc){
        db.local.get(doc._id)
            .then((_doc) => {
                doc._rev = _doc._rev;
                if(JSON.stringify(doc.value) !== JSON.stringify(_doc.value)){
                    db.local.put(doc)
                }
            })
            .catch(() => db.local.put(doc))
    }
}


export const app = {
    bootstrap: function(username){
        //console.log("> BOOTSTRAP")
        if(!username) return;
        if(db.sync) db.sync.cancel();
        if(db.remote) db.remote.close();
        if(db.local) db.local.close();

        function toHex(str) {
            var hex = '';
            for(var i=0; i<str.length; i++) {
                hex += ''+str.charCodeAt(i).toString(16);
            }
            return hex;
        }
        
        db.remote = new PouchDB("http://127.0.0.1:5984/userdb-"+toHex(username));
        db.local = new PouchDB(username);
        db.local.put({
            _id: '_design/query',
            views: {
                receipts: {
                    map: function mapFun(doc) {
                        if (doc.type === "receipt") {
                            emit(doc.description);
                        }
                    }.toString()
                },                
                settings: {
                    map: function mapFun(doc) {
                        if (doc.type === "settings") {
                            emit(doc);
                        }
                    }.toString()
                }
            }
        }).then(() => {}).catch(() => {});

        let getData = _.debounce(function(){
            receipts.get().then((docs) => store.dispatch(importAllRemoteReceipts(docs)))
            settings.get('categories').then((settings) => {
                if(settings) store.dispatch(updateCategories(settings))
            });
            settings.get('currency').then((settings) => {
                if(settings) store.dispatch(updateDefaultCurrency(settings))
            });
        }, 150);
        db.sync = db.local.sync(db.remote, { live: true, retry: true, include_docs: true })
            .on('paused', () => getData())
            .on('error', function (err) {
                if(err.status === 401){
                    store.dispatch(logout());
                    //console.log("> OUPS")
                }else{
                    console.log("ERR", err);
                }
            })
    },
    destroy: function(){
        localStorage.clear();
        _reset();
    }
}



let username = localStorage.getItem("username")
if(username){
    store.dispatch(login(username))
    app.bootstrap(username);
}

