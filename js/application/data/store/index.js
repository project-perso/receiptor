import { applyMiddleware, combineReducers, createStore } from 'redux';
import { settingsReducer } from '../reducers/settings';
import { receiptReducer } from '../reducers/receipts';
import { userReducer } from '../reducers/users';
import { UIReducer } from '../reducers/ui';
import { filterReducer } from '../reducers/filters';

const Logger = (store) => (next) => (action) => {
    next(action);
}


export let store = createStore(combineReducers({
    settings: settingsReducer,
    receipts: receiptReducer,
    ui: UIReducer,
    user: userReducer,
    filter: filterReducer
}));
