import { settings } from '../api';

const defaultSettings = {
    currency: 'AUD',
    categories: [
        'Food',
        'Accomodation',
        'Travel',
        'Equipment',
        'Home'
    ],
    currencies: [
        'AUD',
        'EUR',
        'USD',
        'CAD'
    ]
}

export const settingsReducer = (state = defaultSettings, action) => {
    state = Object.assign({}, state);    
    if(action.type === "SETTINGS_CHANGE_DEFAULT_CURRENCY"){
        state.currency = action.payload.value;
        settings.update(action.payload)
    }else if(action.type === "SETTINGS_CHANGE_UPDATE_CATEGORIES"){
        state.categories = action.payload.value;
        settings.update(action.payload);
    }
    return state;
};

