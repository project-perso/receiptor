const defaultUser = {
    is_loading: false,
    username: null
};

export const userReducer = (state = defaultUser, action) => {
    state = Object.assign({}, state);
    if(action.type === "USER_LOGIN"){
        //console.log("USER LOGIN", action.payload)
        state.is_loading = true;
    }else if(action.type === "USER_LOGIN_SUCCESS"){
        //console.log("USER_LOGIN_SUCCESS", action.payload)
        state.is_loading = false;
        state.username = action.payload
        window.location.hash = "/";
    }else if(action.type === "USER_LOGIN_ERROR"){
        //console.log("USER_LOGIN_ERROR", action.payload)
        state.is_loading = false;
    }else if(action.type === "USER_LOGOUT"){
        //console.log("USER_LOGOUT")
        state.username = null;
        localStorage.clear();
        window.location.hash = "/";
    }
    return state;
}
