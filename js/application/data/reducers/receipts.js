import { receipts } from '../api';

const defaultReceipts = [
    // {_id: "34324", description: "test"},
    // {_id: "343r4", description: "test"},
    // {_id: "343g4", description: "test"},
    // {_id: "34384", description: "test"}
];

export const receiptReducer = (state = defaultReceipts, action) => {
    state = Object.assign([], state);
    
    if(action.type === "RECEIPT_ADD"){
        state.push(action.payload);
        receipts.put(action.payload);
    }else if(action.type === "RECEIPT_UPDATE"){
        state = state.map((receipt) => {            
            return receipt._id === action.payload._id? action.payload : receipt;
        });
        receipts.update(action.payload);
    }else if(action.type === "RECEIPT_DELETE"){
        receipts.delete(action.payload);
        state = state.filter((receipt) => {
            return receipt._id === action.payload? false: true;
        });
    }else if(action.type === "RECEIPT_BULK_IMPORT"){
        state = action.payload;
    }else if(action.type === "USER_LOGOUT"){
        state = []
    }
    return state;
};
