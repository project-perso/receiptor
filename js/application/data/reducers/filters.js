const defaultFilter = {
    search: '',
    layout: 'ORDER_BY_DATE'
}

export const filterReducer = (state = defaultFilter, action) => {
    state = Object.assign({}, state);    
    if(action.type === "FILTER_SEARCH"){
        state.search = action.payload;
    }else if(action.type === "FILTER_LAYOUT"){
        state.layout = action.payload;
    }
    return state;
};
