const defaultUI = {
    is_editing: false,
    current_receipt: {},
    show_settings: false,
    show_stats: false,
}

export const UIReducer = (state = defaultUI, action) => {
    state = Object.assign({}, state);
    if(action.type === "EDITING_RECEIPT"){
        state.is_editing = action.payload;
        if(state.is_editing === true) state.current_receipt = Object.assign({}, action.doc)
    }else if(action.type === "SETTINGS_OPEN"){
        state.show_settings = action.payload || false;
    }else if(action.type === "STATISTICS_OPEN"){
        state.show_stats = action.payload || false;
    }
    return state;
}
