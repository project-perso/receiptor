export function editReceipt(payload, receipt = {}){
    if(typeof payload !== 'boolean') throw("arg has to be a boolean")
    return {
        type: "EDITING_RECEIPT",
        payload: payload,
        doc: receipt
    }
}

export function editSettings(payload){
    if(typeof payload !== 'boolean') throw("arg has to be a boolean")
    return {
        type: "SETTINGS_OPEN",
        payload: payload
    }
}

export function openSettings(payload){
    if(typeof payload !== 'boolean') throw("arg has to be a boolean")
    return {
        type: "SETTINGS_OPEN",
        payload: payload
    }
}


export function openStats(payload){
    if(typeof payload !== 'boolean') throw("arg has to be a boolean")
    return {
        type: "STATISTICS_OPEN",
        payload: payload
    }
}

export function changeLayout(payload){
    if(payload === "ORDER_BY_CATEGORY" || payload == "ORDER_BY_DATE"){
        return {
            type: "FILTER_LAYOUT",
            payload: payload
        }
    }else{
        return {
            type: "FILTER_LAYOUT",
            payload: payload
        }
    }
}

export function search(payload){
    return {
        type: "FILTER_SEARCH",
        payload: payload
    }
}



export function updateCategories(payload){
    let settings = {
        _id: 'categories',
        type: 'settings',
        value: payload
    }
    return {
        type: "SETTINGS_CHANGE_UPDATE_CATEGORIES",
        payload: settings
    }
}
export function updateDefaultCurrency(payload){
    let settings = {
        _id: 'currency',
        type: 'settings',
        value: payload
    }
    return {
        type: "SETTINGS_CHANGE_DEFAULT_CURRENCY",
        payload: settings
    }
}
