export function login(username){
    return {
        type: "USER_LOGIN_SUCCESS",
        payload: username
    };
}

export function logout(){
    return {
        type: "USER_LOGOUT"
    };
}
