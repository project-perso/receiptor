import uuid from 'uuid';

export function importAllRemoteReceipts(receipts){
    return {
        type: "RECEIPT_BULK_IMPORT",
        payload: receipts
    };
}

export function importRemoteReceipts(receipts){
    return {
        type: "RECEIPT_UPDATE_RECEIVE",
        payload: receipts
    };
}

export function saveReceipt(_doc){
    let doc = Object.assign({}, _doc);
    for(let key in doc){
        if(doc[key] === ''){
            doc[key] = null;
        }
    }
    if(doc.amount !== null) doc.amount = Number(doc.amount);
    if(!doc.timestamp) doc.timestamp = [];
    doc.timestamp.push(new Date().toISOString());

    if(doc._id){
        return {
            type: "RECEIPT_UPDATE",
            payload: doc
        }
    }else{
        doc._id = uuid.v1();
        doc.type = "receipt";
        return {
            type: "RECEIPT_ADD",
            payload: doc
        }
    }
}

export function removeReceipt(_doc){
    let doc = Object.assign({}, _doc);
    return {
        type: "RECEIPT_DELETE",
        payload: doc._id
    };
}
