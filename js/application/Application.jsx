'use strict';

import React from 'react';

import { PageReceipts, PageLogin, PageLogout } from './pages/';
import { Api } from './shared/api.jsx';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MyTheme from './shared/theme';

import { BrowserRouter as Router, Link, Route, HashRouter } from 'react-router-dom';

class Application extends React.Component {

    constructor(props) {
        super(props);
    }

    getChildContext() {
        return {muiTheme: getMuiTheme(MyTheme)};
    }

    authRequired(){
        if(this.props.user.logged_in !== true){
            
        }
    }

    render() {
        return (
            <HashRouter>
              <div>
                <Route exact path="/" component={PageReceipts} />
                <Route path="/login" component={PageLogin}/>
                <Route path="/logout" component={PageLogout}/>
              </div>
            </HashRouter>
        );
    }
}

Application.childContextTypes = {
    muiTheme: React.PropTypes.object
};

export default Application;
