'use strict';

import React from 'react';
import { Dialog, FlatButton, Paper } from 'material-ui';
import { openStats } from '../data/actions';
import { connect } from 'react-redux';
import theme from '../shared/theme';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import _ from 'lodash';



class Bubble extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        let arrow = this.props.value > this.props.average? '↗' : '↘';
        return (
            <div style={{width: '100px', height: '100px', borderRadius: '50%', textAlign: 'center', background: '#ffffff', boxShadow: "rgba(158, 163, 172, 0.3) 0px 19px 60px, rgba(158, 163, 172, 0.22) 0px 15px 20px", margin: '5px', display: 'inline-block'}}>         
              <div style={{paddingTop: '19px', marginBottom: '-5px'}}>{this.props.label}</div>
              <div style={{fontSize: '30px', color: theme.palette.textColor}}>{Math.round(this.props.value)}</div>
              <div style={{marginTop: '-5px'}}>
                <span>~{Math.round(this.props.average)}</span>
                <span style={{fontWeight: 'bold'}}>{arrow}</span>
              </div>
            </div>
        );
    }
}


class SummaryChart extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            width: 600 - 15*2
        };
        //window.addEventListener('resize', this._onResize.bind(this), false);
    }

    // _onResize(){
    //     this.setState({width: Number(document.querySelector(".barchart").offsetWidth) });
    // }
    // componentDidMount(){
    //     console.log("> MOUNT");
    // }
    // componentWillUnmount() {
    //     console.log("> UNMOUNT");
    //     window.removeEventListener('resize', this._onResize.bind(this), false);
    // }

    colorChange(col,amt) {
        col = col.replace("#", "");
        var num = parseInt(col,16);
        var r = (num >> 16) + amt;
        var b = ((num >> 8) & 0x00FF) + amt;
        var g = (num & 0x0000FF) + amt;
        var newColor = g | (b << 8) | (r << 16);
        return '#'+newColor.toString(16);
    }

    

    render(){
        let labels = [];
        this.props.data.map((row) => {
            for(let key in row){
                if(key !== 'name' && labels.indexOf(key) === -1){                    
                    labels.push(key);
                }
            }
        });
        return (
            <div style={{padding: '15px 15px 0 15px', marginBottom: '20px'}} className="barchart">
              <BarChart width={this.state.width} height={200} data={this.props.data}
                        margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                <XAxis dataKey="name"/>
                <YAxis hide={true} />
                <Tooltip/>
                {
                    labels.map((label, index) => {
                        return <Bar key={index} dataKey={label} stackId="a" fill={this.colorChange('#333333', index*20)} />;
                    })
                }
              </BarChart>
            </div>            
        );
    }
}


@connect((store) => {
    return {
        open: store.ui.show_stats,
        receipts: store.receipts
    };
})
class Statistics extends React.Component {
    constructor(props) {
        super(props);
        this.state = this._reset(this.props.receipts);
    }

    componentWillReceiveProps(props){
        this.state = null;
        this.setState(this._resetState(props.receipts));
    }

    _reset(receipts){
        const MONTH = [
            "Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.",
            "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."
        ];
        let state = {
            week: {
                sum_current: 0,
                sum_period: 0
            },
            month: {
                sum_current: 0,
                sum_period: 0
            },
            quarter: {
                sum_current: 0,
                sum_period: 0
            },           
            data: function(){
                let data = {};
                MONTH.map((month) => {
                    data[month] = {};
                });
                return data;
            }()
        };
        let now = new Date();
        const QUARTER_PERIOD = 4;
        const MONTH_PERIOD = 4;
        const WEEK_PERIOD = 6;
        let ldate = {
            quarter: new Date(now.getFullYear(), now.getMonth() - 4, 0).toISOString(),
            quarter_period: new Date(now.getFullYear(), now.getMonth() - 4*QUARTER_PERIOD, 0).toISOString(),
            month: new Date(now.getFullYear(), now.getMonth(), 0).toISOString(),
            month_period: new Date(now.getFullYear(), now.getMonth() - 4*MONTH_PERIOD, 0).toISOString(),
            week: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7).toISOString(),
            week_period: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7*WEEK_PERIOD).toISOString(),
            now: now.toISOString()            
        };
        for(let i=0, l=receipts.length; i<l; i++){
            let ds = new Date(receipts[i].date_issued).toISOString();
            let a = receipts[i].amount;
            let c = receipts[i].category || 'Other';
            // statistics data for year/month/day
            if(ldate.now > ds){
                if(ds >= ldate.week){state.week.sum_current += a;}
                if(ds >= ldate.week_period){state.week.sum_period += a;}
                if(ds >= ldate.month){state.month.sum_current += a;}
                if(ds >= ldate.month_period){state.month.sum_period += a;}
                if(ds >= ldate.quarter){state.quarter.sum_current += a;}
                if(ds >= ldate.quarter_period){state.quarter.sum_period += a;}
            }

            // data for the bar chart
            let month = new Date(receipts[i].date_issued).getMonth();
            let row = state.data[MONTH[month]];
            row[c] = row[c] >= 0 ? row[c]+a : a;
        }

        return {
            stats: {
                week: {value: state.week.sum_current, avg: state.week.sum_period / WEEK_PERIOD},
                month: {value: state.month.sum_current, avg: state.month.sum_period / MONTH_PERIOD},
                quarter: {value: state.quarter.sum_current, avg: state.quarter.sum_period / QUARTER_PERIOD}
            },
            data: function(data){
                let m = new Date().getMonth();
                var chart = [];
                for(let i=m; i<m+12; i++){
                    let tmp = data[MONTH[Math.abs((i - 11)) % 12]];
                    tmp.name = MONTH[Math.abs((i - 11)) % 12];
                    chart.push(tmp);
                }
                return chart;
            }(state.data)
        };
    }

    componentWillReceiveProps(props) {
        this.setState(this._reset(props.receipts));
    }


    onClose(){
        this.props.dispatch(openStats(false));
    }
    
    render() {
        const actions = [
                <FlatButton
            label="Close"
            onClick={this.onClose.bind(this)}
            primary={true}
                />
        ];
        return (
            <Dialog actions={actions} modal={false} open={this.props.open} onRequestClose={this.onClose.bind(this)} actionsContainerStyle={{background: '#f2f2f2'}} bodyStyle={{background: '#f2f2f2', padding: '0'}}>
              <SummaryChart data={this.state.data} />
              <div style={{textAlign: 'center', marginBottom: '30px'}}>
                <Bubble label="week" value={this.state.stats.week.value} average={this.state.stats.week.avg} />
                <Bubble label="month" value={this.state.stats.month.value} average={this.state.stats.quarter.avg} />
                <Bubble label="quarter" value={this.state.stats.quarter.value} average={this.state.stats.quarter.avg} />
              </div>
            </Dialog>
        );
    }
}

export default Statistics;
