'use strict';

import React from 'react';
import { connect } from 'react-redux';

import { editReceipt } from '../data/actions';


import FloatingActionButton from 'material-ui/FloatingActionButton';
import Divider from 'material-ui/Divider';
import ContentSave from 'material-ui/svg-icons/content/save';
import { ExpandableBottomSheet, BottomSheet } from 'material-ui-bottom-sheet';
import TextField from 'material-ui/TextField';
import { List, ListItem, Subheader } from 'material-ui';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const styles = {
    fab: {
        position: 'fixed',
        bottom: '20px',
        right: '20px'
    },
    input: {
        marginLeft: 20
    }
};


@connect((store) => {
    return {
        is_editing: store.ui.is_editing,
        receipt: store.ui.current_receipt
    };
})
class Editor extends React.Component {

    constructor(props){
        super(props);      
    }

    componentWillMount(){
        console.log("test");
        this.props.dispatch(editReceipt(true));
    }
    
    _onClose(e){
        this.props.dispatch(editReceipt(false));
    }
    
    render(){
        return (
            <BottomSheet open={this.props.is_editing}>
              <FloatingActionButton onClick={this._onClose.bind(this)} style={{float: 'right', marginRight: '16px', marginTop: '-28px'}}>
                <ContentSave />
              </FloatingActionButton>
              <Subheader>{this.props.receipt.id ? 'Edit' : 'Add'}</Subheader>
              <div style={{padding: '0 20px 20px 20px'}}>                
                <TextField hintText="Description" type="text" underlineShow={false} />
                <Divider />
                <TextField hintText="Amount" type="text" underlineShow={false} type="number" />
                <Divider />
              </div>
            </BottomSheet>
        )
    }
}


export default Editor;
