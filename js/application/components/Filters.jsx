'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { RaisedButton, TextField, FlatButton, AutoComplete, Divider, SelectField, MenuItem }from 'material-ui';
import { changeLayout, search } from '../data/actions/';

@connect((store) => {
    return {
        layout: store.filter.layout,
        search: store.filter.search
    };
})
class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            from: new Date(),
            to: new Date(),
            dataSource: []
        };
    }

    onUpdateInput(value){
        this.props.dispatch(search(value));
        //let state = Object.assign([], this.state);
        // state.dataSource = [
        //     value,
        //     value + value + value,
        //     value + value + value + value,
        //     "testing",
        //     value + value,
        //     value + value + value + value,
        // ];
        //this.setState(state);
    }

    onFilterChange(e, index, value){
        //console.log(e, index, value);
        this.props.dispatch(changeLayout(value));
    }    

    render() {
        return (
            <div>
              <AutoComplete
                underlineShow={false}
                dataSource={this.state.dataSource}
                onUpdateInput={this.onUpdateInput.bind(this)}
                hintText="Search"
                fullWidth={true}
                value={this.props.search}
                style={{width: "calc(100% - 130px)"}}
                />
              <SelectField onChange={this.onFilterChange.bind(this)} value={this.props.layout} hintText="Category" style={{width: "130px", verticalAlign: 'bottom'}} underlineShow={false}>
                <MenuItem value={'ORDER_BY_DATE'} primaryText="Sort by Date" />
                <MenuItem value={'ORDER_BY_CATEGORY'} primaryText="Sort By Category" />
              </SelectField>                
              <Divider style={{marginBottom: "20px"}}/>
            </div>            
        );
    }
}

// Filters.propTypes = {
// };

export default Filters;
