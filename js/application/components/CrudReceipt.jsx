'use strict';

import React from 'react';

import Dropzone from 'react-dropzone';
import request from 'superagent';

import { connect } from 'react-redux';
import { editReceipt, saveReceipt, removeReceipt } from '../data/actions';
import Container from './Container.jsx';

import ContentSave from 'material-ui/svg-icons/content/save';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentDelete from 'material-ui/svg-icons/action/delete';
import { FloatingActionButton, RaisedButton, FlatButton, Divider, DatePicker, TextField, MenuItem, SelectField, List, ListItem, Subheader, CircularProgress } from 'material-ui';
import { ExpandableBottomSheet, BottomSheet } from 'material-ui-bottom-sheet';
import ReactBottomSheet from 'react-bottomsheet';


@connect()
class AddButton extends React.Component {
    constructor(props){
        super(props);
    }

    onClick(){
        this.props.dispatch(editReceipt(true));
    }
    
    render(){
        return (
            <FloatingActionButton onClick={this.onClick.bind(this)} style={{position: 'fixed', bottom: '20px', right: '20px'}}>
              <ContentAdd />
            </FloatingActionButton>
        );
    }
}

@connect((store) => {
    return {
        is_editing: store.ui.is_editing,
        receipt: store.ui.current_receipt,
        categories: store.settings.categories,
        currencies: store.settings.currencies,
        default_currency: store.settings.default_currency
    };
})
class Editor extends React.Component {

    constructor(props){
        super(props);
        this.state = this._resetState();
    }

    componentWillReceiveProps(props){
        this.state = null;
        this.setState(this._resetState(props.receipt));
    }

    _resetState(receipt = {}){
        receipt._attachments = receipt._attachments || {};
        receipt.description = receipt.description || '';
        receipt.amount = receipt.amount || '';
        receipt.category = receipt.category || '';
        receipt.currency = receipt.currency || 'AUD';
        receipt.date_issued = receipt.date_issued? new Date(receipt.date_issued) : new Date();
        return receipt;
    }
   
    _onSave(){
        this.props.dispatch(saveReceipt(this.state));
        this.props.dispatch(editReceipt(false));
    }

    _onDelete(){
        this.props.dispatch(removeReceipt(this.state));
        this.props.dispatch(editReceipt(false));
    }

    _onClose(e){
        let $root = e.target.parentElement.parentElement.parentElement.parentElement;
        if($root.hasAttribute('data-reactroot')){
            e.preventDefault();
            this.props.dispatch(editReceipt(false));
        }
    }

    _onChange(field, e, index, value){
        let state = Object.assign({}, this.state);
        state[field] = value || index;
        this.setState(state);
    }

    _onImageDrop(files){
        let state = Object.assign({}, this.state);
        let file = files[0];
        var reader = new FileReader();
        reader.onload = () =>{
            state._attachments[file.name] = {
                content_type: file.type,
                data: btoa(reader.result)
            };
            this.setState(state);
        };
        reader.readAsBinaryString(file);
    }

    _onImageDelete(filename){
        let state = Object.assign({}, this.state);
        delete state._attachments[filename];
        this.setState(state);
    }
    
    render(){
        let deletebutton = null;
        if(this.state._id !== null){
            deletebutton = <RaisedButton secondary={true} onClick={this._onDelete.bind(this)} label="DELETE" style={{width: '100%', marginTop: '15px'}} />;
        }
        let attachments = Object.keys(this.state._attachments).map((filename, index) => {
            return (
                <div key={index} className="wrapper" style={{position: 'relative'}}>
                  <div style={{position: 'absolute', top: '10px', right: '10px', pointer: 'cursor'}} onClick={this._onImageDelete.bind(this, filename)}>
                    <FloatingActionButton mini={true}>
                      <ContentDelete/>
                    </FloatingActionButton>
                  </div>
                  <Preview data={this.state._attachments[filename].data} type={this.state._attachments[filename].content_type}/>
                </div>
            );                
        });
        return (
            <ExpandableBottomSheet open={this.props.is_editing} onRequestClose={this._onClose.bind(this)} action={<FloatingActionButton onClick={this._onSave.bind(this)} style={{float: 'right', marginRight: '16px', marginTop: '-28px'}}><ContentSave/></FloatingActionButton>}>
              <Container>
                <Subheader>{this.state._id ? 'Edit' : 'Add'}</Subheader>
                <form onSubmit={this._onSave.bind(this)} style={{padding: '0 15px 15px 15px'}}>
                  <SelectField onChange={this._onChange.bind(this, 'category')} value={this.state.category} hintText="Category" style={{width: "130px", verticalAlign: 'bottom'}} underlineShow={false}>
                    {
                        this.props.categories.map((category) => {
                            return <MenuItem key={category} value={category} primaryText={category}/>;
                        })
                    }
                  </SelectField>                
                  <TextField onChange={this._onChange.bind(this, 'description')} value={this.state.description} hintText="Description" type="text" style={{width: "calc(100% - 130px)"}} underlineShow={false} autoFocus />
                  <Divider />
                  <SelectField onChange={this._onChange.bind(this, 'currency')} value={this.state.currency} hintText="Currency" style={{width: "88px", verticalAlign: 'bottom'}} underlineShow={false}>
                    {
                          this.props.currencies.map((currency) => {
                              return <MenuItem key={currency} value={currency} primaryText={currency}/>;
                          })
                    }
                  </SelectField>
                  <TextField onChange={this._onChange.bind(this, 'amount')} value={this.state.amount} hintText="Amount" type="number" style={{width: "calc(50% - 88px)"}} underlineShow={false} />
                  <DatePicker onChange={this._onChange.bind(this, 'date_issued')} value={this.state.date_issued} hintText="Issuing date" mode="portrait" autoOk={true} style={{display: "inline-block", width: "50%"}} underlineShow={false} />
                  <Divider />
                  <div className="receipt-container">
                    <div className="wrapper">
                      <Dropzone
                        className="receipt-add"
                        onDrop={this._onImageDrop.bind(this)}>
                        <ContentAdd className="icon"/>
                      </Dropzone>
                    </div>
                    {attachments}
                  </div>
                  <Divider />
                  {deletebutton}
                </form>
              </Container>
            </ExpandableBottomSheet>            
        );
    }
}


class Preview extends React.Component{
    constructor(props){super(props);}

    download(attachment){
        let url = `data:${this.props.type};base64,${this.props.data}`;
        window.open(url,'_blank');
    }


    render(){
        if(this.props.data && /^image\/.*$/.test(this.props.type)){
            let url = `data:${this.props.type};base64,${this.props.data}`;
            return <div onClick={this.download.bind(this)} className="receipt-preview" style={{backgroundImage: `url("${url}")`}}></div>;
        }else{
            return <div onClick={this.download.bind(this)} className="receipt-preview" style={{backgroundImage: `url("http://mickael.kerjean.free.fr/public/apps/receipt/image.png")`, backgroundSize: 'contain', backgroundRepeat: 'no-repeat', backgroundPosition: '50%'}}></div>;
        }
    }    
}

class CRUDReceipt extends React.Component {
    constructor(props){
        super(props);        
    }

    render(){
        return (
            <div>
              <AddButton/>
              <Editor/>
            </div>
        );
    }
}
export default CRUDReceipt;
