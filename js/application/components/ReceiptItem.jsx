'use strict';

import React from 'react';

import { Paper, Chip, FlatButton, ListItem, Divider, TextField, Checkbox, IconButton, Card, CardActions, CardHeader, CardText, Avatar } from 'material-ui';
import FileFolder from 'material-ui/svg-icons/file/folder';

import { editReceipt } from '../data/actions';
import { connect } from 'react-redux';

class AmountIcon extends React.Component {
    constructor(props){
        super(props);
        this.state = this._resetState();
    }
    
    componentWillReceiveProps(props){
        this.setState(this._resetState());
    }

    _resetState(){
        return {
            currency: this.props.currency,
            amount: this.props.amount
        };
    }


    display(amount, currency){
        if(!amount) { return '';}
        if(currency === "EUR"){
            return amount + "€";
        }else if(["USD", "AUD", "CAD"].indexOf(currency)){
            return '$'+amount;
        }else{
            return amount+" "+currency;
        }
    }

    render(){
        return (
            <div style={{position: 'absolute', right: '16px', bottom: '16px'}}>
              {this.display(this.state.amount, this.state.currency)}
            </div>
        );
    }
}
AmountIcon.propTypes = {
    amount: React.PropTypes.number,
    currency: React.PropTypes.string
};

class DateIcon extends React.Component {
    constructor(props){
        super(props);
        let date = new Date(this.props.date);
        this.state = this._resetState();
    }

    componentWillReceiveProps(props){
        this.setState(this._resetState());
    }

    _resetState(){
        let date = new Date(this.props.date);
        return {
            day: this._findDay(date),
            weekday: this._findWeekDay(date),
            year: this._findYear(date),
            month: this._findMonth(date)
        }; 
    }


    _findDay(date){
        let day = String(date.getDate());
        let pad = "00";
        return pad.substring(0, pad.length - day.length) + day;        
    }
    _findWeekDay(date){
        return ["Mon.", "Tue.", "Wed.", "Thu.", "Fri.", "Sat.", "Sun."][date.getDay()];
    }
    _findYear(date){
        return date.getFullYear();
    }
    _findMonth(date){
        return ["Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.",
                "Jul.", "Aug.", "Sept.", "Nov.", "Dec."][date.getMonth()];
    }

    render(){
        return (
            <div style={{textAlign: 'center', height: '72px', 'width': '72px', position: 'absolute', top: '0px', left: '0px', borderTopLeftRadius: '2px', borderBottomLeftRadius: '2px'}}>
              <div style={{paddingTop: '9px', paddingBottom: '3px', fontSize: '14px'}}>{this.state.weekday}</div>
              <div style={{fontSize: '30px'}}>{this.state.day}</div>
              <div style={{paddingTop: '3px'}}>
                <span style={{fontSize: '12px'}}>{this.state.month} </span>
                <span style={{fontSize: '10px'}}>{this.state.year}</span>
              </div>
            </div>
        );
    }
}
// DateIcon.propTypes = {
//     date: React.PropTypes.string
// };


@connect((store) => {
    return {
        search: store.filter.search
    };
})
class ReceiptItem extends React.Component {
    constructor(props) {
        super(props);      
    }

    onClick(){
        this.props.dispatch(editReceipt(true, this.props.receipt));
    }

    formatDate(d){
        let date = new Date(d);        
        return date.getDate() + date.getFullYear();
    }

    shouldAppear(terms, receipt){
        terms = terms.trim();
        let text_search = [receipt.description, receipt.category].join(" ");
        let appear = false;
        if(terms === "") return true;
        else{
            terms = terms.split(" ");
            let matchs = terms.filter((term) => {
                if(/>[\d]*/.test(term)){
                    return receipt.amount > />([\d]*)/.exec(term)[1];
                }else if(/<[\d]*/.test(term)){
                    return receipt.amount < /<([\d]*)/.exec(term)[1];
                }else{
                    return text_search.indexOf(term) === -1 ? false : true;
                }                
            });
            if(matchs.length === terms.length){
                appear = true;
            }
        }
        return appear;
    }
  
    render() {
        //console.log(this.props.search);
        if(this.shouldAppear(this.props.search, this.props.receipt)){
            return (
                <Paper className="receipt-element" onClick={this.onClick.bind(this)} zDepth={this.props.depth || 5} style={{marginBottom: '5px', cursor: 'pointer'}}>
                  <ListItem
                    innerDivStyle={{paddingLeft: '88px'}}
                    leftAvatar={<DateIcon date={this.props.receipt.date_issued} />}
                    rightAvatar={<AmountIcon amount={this.props.receipt.amount} currency={this.props.receipt.currency} />}
                    primaryText={this.props.receipt.description || '--'}
                    secondaryText={this.props.receipt.category || 'Other'}
                    />
                </Paper>
            );
        }else{
            return null;
        }       
    }
}

ReceiptItem.propTypes = {
    receipt: React.PropTypes.object   
};


export default ReceiptItem;
