'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const styles = {
    maxWidth: '800px',
    margin: '0px auto 0 auto',
    padding: '10px 10px 10px'    
}

class Container extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={ styles }>
              { this.props.children }
            </div>
        );
    }
}

//Container.propTypes = {children: PropTypes.object };

export default Container;
