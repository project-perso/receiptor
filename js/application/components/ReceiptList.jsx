'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import { List, Subheader, ListItem, Divider}from 'material-ui';
import ReceiptItem from './ReceiptItem.jsx';

import { connect } from 'react-redux';

@connect((store) => {
    return {
        receipts: store.receipts,
        layout: store.filter.layout,
        categories: store.settings.categories
    };
})
class ReceiptList extends React.Component {
    constructor(props) {
        super(props);
    }

    onPropsChange(){
        let receipts = this.props.receipts;        
    }

    generateLayout(receipts, layout){
        if(layout === "ORDER_BY_CATEGORY"){
            return this._generateLayoutOrderByCategory(receipts);
        }else{
            return this._generateLayoutOrderByDate(receipts);
        }
    }
    _generateLayoutOrderByCategory(_receipts){
        let categories = this.props.categories.reduce((acc, el) => {
            acc[el] = [];
            return acc;
        }, {"null": []});
        var receipts = _receipts
            .sort((receiptA, receiptB) => {
                return receiptA.date_issued < receiptB.date_issued;
            })
            .reduce((acc, el) => {
                let cats = Object.keys(acc);
                if(el.category === null){
                    acc["null"].push(el);
                    return acc;
                }
                for(let i = 0, l=cats.length; i<l; i++){
                    if(cats[i] === el.category){
                        acc[cats[i]].push(el);
                        return acc;
                    }
                }
                return acc;
            }, categories);

        let keys = Object
            .keys(receipts)
            .sort((keyA, keyB) => {
                if(keyA === 'null') return 1;
                return keyA > keyB;
            });
       
        return (
            <div>
            {
                keys.map((key) => {
                    if(receipts[key].length > 0){
                        return (
                            <div key={key}>
                              <Subheader style={{fontSize: '17px', textTransform: 'capitalize', fontWeight: 'normal', fontStyle: 'italic', lineHeight: 'inherit'}}>{key === "null"? "Other" : key} ({receipts[key].length})</Subheader>
                              <List style={{marginBottom: '20px', display: 'flex', overflowX: 'scroll'}}>
                                {
                                    receipts[key].map((receipt, index) => {                      
                                        return (
                                            <div key={receipt._id} style={{padding: '0 8px', minWidth: '300px', maxWidth: '350px'}}>
                                              <ReceiptItem depth={2} receipt={ receipt }/>
                                            </div>
                                        );
                                    })
                                }
                            </List>
                                </div>
                        );
                    }else{
                        return null;
                    }
                })
            }
            </div>
        );
    }

    _generateLayoutOrderByDate(_receipts){
        let receipts = _receipts.sort((receiptA, receiptB) => {
            return receiptA.date_issued < receiptB.date_issued;
        });
        return (
            <List>
              {
                  receipts.map((receipt, index) => {                      
                      return (
                          <ReceiptItem key={receipt._id} receipt={ receipt }/>
                      );
                  })
              }
            </List>
        );
    }

    render() {
        var content = this.generateLayout(this.props.receipts, this.props.layout);
        return (
            <div>
              {content}
            </div>
        );
    }
}

ReceiptList.propTypes = {
    receipts: PropTypes.array
};

export default ReceiptList;
