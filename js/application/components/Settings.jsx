import React from 'react';
import { connect } from 'react-redux';
import { updateCategories, editSettings, updateDefaultCurrency } from '../data/actions/';
import { List, ListItem, Subheader, Toggle, TextField, SelectField, MenuItem, FloatingActionButton }  from 'material-ui';
import { ExpandableBottomSheet, BottomSheet } from 'material-ui-bottom-sheet';
import ContentSave from 'material-ui/svg-icons/content/save';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import Container from './Container.jsx';

@connect((store) => {
    return {
        show_settings: store.ui.show_settings,
        categories: store.settings.categories,
        currencies: store.settings.currencies,
        default_currency: store.settings.currency
    };
})
class Settings extends React.Component {
    
    constructor(props){
        super(props);
        this.state = this._resetState();
    }

    componentWillReceiveProps(props){
        this.setState(this._resetState());
    }

    _resetState(){
        return {
            category: '',
            categories: this.props.categories,
            default_currency: this.props.default_currency
        };
    }

    onChange(field, e, index, value){
        let state = Object.assign({}, this.state);
        state[field] = value || index;
        this.setState(state);
    }

    onNewCategory(e){
        e.preventDefault();
        let state = Object.assign({}, this.state);
        state.categories.push(this.state.category);
        state.category = '';
        this.setState(state);
    }
    onDeleteCategory(_category){
        let state = Object.assign({}, this.state);
        state.categories = state.categories.filter((category) => {
            return category === _category? false: true;
        });
        this.setState(state);
    }

    onClose(e){
        let $root = e.target.parentElement.parentElement.parentElement;
        if($root.hasAttribute('data-reactroot')){
            e.preventDefault();
            this.props.dispatch(editSettings(false));
        }
    }

    onSave(){
        this.props.dispatch(updateCategories(this.state.categories));
        this.props.dispatch(updateDefaultCurrency(this.state.default_currency));
        this.props.dispatch(editSettings(false));
    }
    
    render(){
        return (            
            <ExpandableBottomSheet onRequestClose={this.onClose.bind(this)} open={this.props.show_settings} action={<FloatingActionButton onClick={this.onSave.bind(this)}><ContentSave/></FloatingActionButton>}>
              <Container>
                <Subheader>Categories</Subheader>
                <List style={{padding: 0}}>
                  <ListItem style={{paddingTop: 0, paddingBottom: 0}} className="list-padding">
                    <form onSubmit={this.onNewCategory.bind(this)}>
                      <TextField value={this.state.category} onChange={this.onChange.bind(this, 'category')} hintText="Add a new category ..." fullWidth={true}/>
                    </form>
                  </ListItem>
                  {
                      this.state.categories.map((category) => {
                          return <ListItem key={category} primaryText={category} rightIcon={<ContentRemove onClick={this.onDeleteCategory.bind(this, category)} />} />;
                      })
                  }
                </List>
                <Subheader style={{marginTop: '40px'}}>Default Currency</Subheader>
                <List style={{padding: 0}}>
                  <ListItem style={{paddingTop: 0, paddingBottom: 0}} className="list-padding">
                    <SelectField onChange={this.onChange.bind(this, 'default_currency')} value={this.state.default_currency} hintText="Default Currency" fullWidth={true}>
                      {
                          this.props.currencies.map((currency) => {
                              return <MenuItem key={currency} value={currency} primaryText={currency}/>;
                          })
                      }
                    </SelectField>
                  </ListItem>
                </List>
              </Container>
            </ExpandableBottomSheet>            
        );
    }
}

export default Settings;
