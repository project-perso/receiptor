'use strict';

import React from 'react';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';



import uuid from 'uuid';
import { isFunction, trim } from 'lodash';

const styles = {
    form: {
        verticalAlign: 'bottom',
    },
    amount: {
        maxWidth: '90px'
    },
    description: {
        
    },
    currency: {
        maxWidth: '90px',
        verticalAlign: 'botton'
    },
    category: {
        verticalAlign: 'bottom',
        maxWidth: '150px'
    }
}

class TodoInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: 'testing',
            currency: '',
            amount: 0,
            category: '',
            attachments: [],
            date_created: new Date(),
            date_issued: new Date(),
            isExpanded: false
        };
    }

    // _onSubmit(e) {
    //     if(e.nativeEvent.keyCode !== 13){
    //         return;
    //     }
    //     if (isFunction(this.props.onTodoAdded) && trim(this.state.value)) {
    //         this.props.onTodoAdded({
    //             id: uuid.v1(),
    //             text: this.state.value
    //         });
    //     }
    //     this.setState({value: ''});
    // }
    _onSubmit(e){
        e.preventDefault();
        console.log("> SUBMIT")
        console.log(this.state);
    }

    _valueChange(e, type) {
        console.log(e.target.description)
        this.setState({description: e.target.description});
    }

    render() {
        let inputStyle = {
            padding: '16px 16px 11px 60px',
            boxSizing: 'border-box'
        };
        let underlineStyle = {marginLeft: '-60px', bottom: '0px'};

        return (
            <form onSubmit={this._onSubmit.bind(this)}>
              <SelectField hintText="Category" value={this.state.category} onChange={this._valueChange.bind(this)}
                           style={ styles.category }
                           >
                <MenuItem value={1} primaryText="Travel" />
                <MenuItem value={2} primaryText="Food" />
                <MenuItem value={3} primaryText="Equipment" />
              </SelectField>
              <TextField value={this.state.amount} onChange={this._valueChange.bind(this, '')} style={ styles.amount } hintText="amount" type="number" />
              <TextField value={this.state.description} style={ styles.description } hintText="Description" />
              <TextField style={ {verticalAlign: 'bottom', maxWith: '100px'} } hintText="receipts" type="file" />
              <button type="submit" style={{display: 'none'}}/>
            </form>
        );
    }
}

TodoInput.propTypes = {onTodoAdded: React.PropTypes.func};

export default TodoInput;
