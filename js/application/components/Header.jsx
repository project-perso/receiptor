'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { logout, editSettings, openStats } from '../data/actions/';
import { AppBar, IconButton, MenuItem, IconMenu } from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { Logo } from '../shared/logo.jsx';

import event from 'Event';
import _ from 'lodash';
import UI from 'UI';

@connect((store) => {
    return {
        receipts: store.receipts
    };
})
class Menu extends React.Component {
    logout(){
        this.props.dispatch(logout());
    }

    export(){
        var csvContent = "data:text/csv;charset=utf-8,";
        this.props.receipts.forEach(function(receipt, index){
            let dataString = `${receipt.description},${receipt.category},${receipt.amount},${receipt.currency},${receipt.date_issued},`;
            if(!receipt.attachments) receipt.attachments = [];
            dataString += receipt.attachments.join(",");
            csvContent += dataString+ "\n";
        });
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("target", "_blank");
        link.setAttribute("download", "details.csv");
        document.body.appendChild(link);
        window.setTimeout(() => {
            document.querySelector("a").click();
        }, 1000);
    }

    settings(){
        this.props.dispatch(editSettings(true));
    }

    stats(){
        this.props.dispatch(openStats(true));
    }

    render(){
        return (
            <IconMenu iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}>
              <MenuItem onClick={this.stats.bind(this)} primaryText="Statistics" />
              <MenuItem onClick={this.settings.bind(this)} primaryText="Settings" />
              <MenuItem onClick={this.export.bind(this)} primaryText="Export" />
              <MenuItem onClick={this.logout.bind(this)} primaryText="Logout" />
            </IconMenu>
        );        
    }
}

class Header extends React.Component {

    constructor(props) {
        super(props);      
    }

    render() {
        return (
            <AppBar title={<span style={{fontSize: '19px'}}>Receiptor</span>}
                    iconElementLeft={<Logo style={{padding: '7px 0', width: "31px"}} />}
                    iconElementRight={<Menu/>}
                    zDepth={ 2 } />
            );
    }
}

export default Header;
