# Introduction

Receiptor is a basic app to store receipts and track expanses. I did that because:
- I didn't want to give my data to third party companies
- I wanted a convenient way to store my receipts as soon as I get them
- I'm bored to see my salary going away without knowing how every cent was spent

# Demo
Try before you install

# Features
- crud your expanses
- work offline and sync when you're back online
- real time sync accross multipe devices
- basic stats
- csv export
- you own your data
- basic search with a few hidden features like (<500 to see expanses that are lower than 500, ...)


# Installation
Install in your server in 2 seconds:
```
apt-get install docker.io docker-compose
docker-compose up -d
```
Not there really is nothing more


# Contributions
Feel free to contribute if you find some use in this
